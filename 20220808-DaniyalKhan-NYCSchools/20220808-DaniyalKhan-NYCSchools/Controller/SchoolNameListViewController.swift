//
//  SchoolNameListViewController.swift
//  20220808-DaniyalKhan-NYCSchools
//
//  Created by Admin on 8/8/22.
//

import Foundation
import UIKit

class SchoolNameListViewController: UIViewController {
    
    let viewModel = SchoolNameListViewModel()
    var schoolData = [SchoolNameListDataModel]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Schools Name"
        
        viewModel.schoolDataCall = { [weak self] data in
            self?.schoolData = data
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
            
        }
    }
    
    
}

extension SchoolNameListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolCell", for: indexPath) as? SchoolCell else { return UITableViewCell()}
        
        let content  = schoolData[indexPath.row]
        cell.school = content
        cell.detailsButtonLabel.tag = indexPath.row
        cell.detailsButtonLabel.addTarget(self, action: #selector(didTapDetailButton(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func didTapDetailButton(sender: UIButton) {
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        
        let content = schoolData[indexPath.row]
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc =  storyboard.instantiateViewController(withIdentifier: "SchoolSatViewController") as? SchoolSatViewController else {return}
       
        vc.name = content.school_name!
        vc.math = content.satScorces?.sat_math_avg_score ??  "no scorce found"
        vc.reading = content.satScorces?.sat_critical_reading_avg_score ??  "no scorce found"
        vc.writing = content.satScorces?.sat_writing_avg_score ??  "no scorce found"
        vc.email = content.school_email!
        vc.phoneNumber = content.phone_number ?? "no number found"
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}
