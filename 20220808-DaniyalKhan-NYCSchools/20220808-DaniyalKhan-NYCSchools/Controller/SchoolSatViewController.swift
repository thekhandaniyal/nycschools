//
//  SchoolSatViewController.swift
//  20220808-DaniyalKhan-NYCSchools
//
//  Created by Admin on 8/8/22.
//

import UIKit

class SchoolSatViewController: UIViewController {

    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mathLabel: UILabel!
    @IBOutlet weak var readingLabel: UILabel!
    @IBOutlet weak var writingLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    var name = "no Name found"
    var math = "no scorce found"
    var reading = "no scorce found"
    var writing = "no scorce found"
    var phoneNumber = "no Number Found"
    var email = "no email found"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.nameLabel.text = name
        self.mathLabel.text = "Math Score:- \(math)"
        self.readingLabel.text = "Reading Score:- \(reading)"
        self.writingLabel.text = "Writing Score:- \(writing)"
        self.phoneNumberLabel.text = "Phone Number:- \(phoneNumber)"
        self.emailLabel.text = "Email:- \(email)"
        
    }
    
    
}
