//
//  SchoolNameListDataModel.swift
//  20220808-DaniyalKhan-NYCSchools
//
//  Created by Admin on 8/8/22.
//

import Foundation

struct SchoolNameListDataModel: Decodable {
    
    let dbn: String?
    let school_name: String?
    let phone_number: String?
    let school_email: String?
    
    var satScorces: SchoolSatScorces?
}

