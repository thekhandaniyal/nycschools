//
//  SchoolSatScorces.swift
//  20220808-DaniyalKhan-NYCSchools
//
//  Created by Admin on 8/8/22.
//

import Foundation

struct SchoolSatScorces: Decodable {
    
    let dbn: String?
    let school_name: String?
    let num_of_sat_test_takers: String?
    let sat_critical_reading_avg_score: String?
    let sat_math_avg_score: String?
    let sat_writing_avg_score: String?
    
}

