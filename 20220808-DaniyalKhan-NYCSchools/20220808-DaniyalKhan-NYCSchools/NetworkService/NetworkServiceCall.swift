//
//  NetworkServiceCall.swift
//  20220808-DaniyalKhan-NYCSchools
//
//  Created by Admin on 8/8/22.
//

import Foundation

class NetworkServiceCall {
    
    
    static let shared = NetworkServiceCall()
    
    internal init() {
        
    }
    
    func callApi<T: Decodable>(urlString: String, expecting: T.Type, completion: @escaping (Result<T,Error>) -> ()) {
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            if let error = error {
                print(error.localizedDescription)
            }else if let data = data {
                
                do{
                    let result = try JSONDecoder().decode(expecting, from: data)
                    completion(.success(result))
                }catch {
                    print(error.localizedDescription)
                    completion(.failure(error))
                }
            }
        }
        .resume()
    }
    
}
