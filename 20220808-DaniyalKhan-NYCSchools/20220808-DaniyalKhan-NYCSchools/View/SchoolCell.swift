//
//  SchoolCell.swift
//  20220808-DaniyalKhan-NYCSchools
//
//  Created by Admin on 8/8/22.
//

import Foundation
import UIKit

class SchoolCell: UITableViewCell {
    
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var detailsButtonLabel: UIButton!
    @IBOutlet weak var emailLabel: UILabel!
    
    var school: SchoolNameListDataModel! {
        didSet {
            nameLabel.text = "School Name:- \(school.school_name ?? "No Name Found")"
            emailLabel.text = "School Email:- \(school.school_email ?? "No Email Found")"
        }
    }
    
}
