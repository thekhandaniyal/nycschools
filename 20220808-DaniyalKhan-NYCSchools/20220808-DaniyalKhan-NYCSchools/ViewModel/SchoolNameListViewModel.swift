//
//  SchoolNameListViewModel.swift
//  20220808-DaniyalKhan-NYCSchools
//
//  Created by Admin on 8/8/22.
//

import Foundation

class SchoolNameListViewModel {
    
    let schoolNameUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    let schoolSatScorceUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    
    var schoolData = [SchoolNameListDataModel]()
    
    var schoolDataCall: (([SchoolNameListDataModel]) -> ())?
    
    init() {
        callSchoolNameApi()
    }
    
    func callSchoolNameApi() {
        NetworkServiceCall.shared.callApi(urlString: schoolNameUrl, expecting: [SchoolNameListDataModel].self) { result in
            
            switch result {
            case .success(let data):
                self.schoolData = data
                self.callSchoolSatApi()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func callSchoolSatApi() {
        
        NetworkServiceCall.shared.callApi(urlString: schoolSatScorceUrl, expecting: [SchoolSatScorces].self) { result in
            
            switch result {
                
            case .success(let data):
                self.matchSchoolWithDbn(satScorces: data)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    
    func matchSchoolWithDbn(satScorces: [SchoolSatScorces]) {
        let newSchoolData = schoolData
        schoolData.removeAll()
        
        for satScorce in satScorces {
            if let dbn = satScorce.dbn {
                var matchSchool = newSchoolData.first { nycSchools in
                    return dbn == nycSchools.dbn
                }
                
                guard matchSchool != nil else {
                    continue
                }
                matchSchool?.satScorces = satScorce
                schoolData.append(matchSchool!)
            }
            
        }
        guard let schoolDataCall = schoolDataCall else {
            return
        }

        schoolDataCall(schoolData)
    }
    
}
