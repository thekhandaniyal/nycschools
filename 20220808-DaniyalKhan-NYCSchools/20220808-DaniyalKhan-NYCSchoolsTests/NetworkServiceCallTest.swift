//
//  _0220808_DaniyalKhan_NYCSchoolsTests.swift
//  20220808-DaniyalKhan-NYCSchoolsTests
//
//  Created by Admin on 8/8/22.
//

import XCTest
@testable import _0220808_DaniyalKhan_NYCSchools

class NetworkServiceCallTest: XCTestCase {
    
    func testFetchSchoolNames() {
      
        let schoolNameUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        let exp = expectation(description: "wait for network call")
        
        
        NetworkServiceCall.shared.callApi(urlString: schoolNameUrl, expecting: [SchoolNameListDataModel].self) { result in
            switch result {
                
            case .success(let data):
                XCTAssertNotNil(data)
                exp.fulfill()
            case .failure(let error):
                XCTAssertNil(error)
                exp.fulfill()
            }
        }
        waitForExpectations(timeout: 3.0)
    }
    
    
    func testFetchSchoolSatScorces() {
        let schoolSatScorceUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
        
        let exp = expectation(description: "wait for network call")
        
        
        NetworkServiceCall.shared.callApi(urlString: schoolSatScorceUrl, expecting: [SchoolSatScorces].self) { result in
            switch result {
                
            case .success(let data):
                XCTAssertNotNil(data)
                exp.fulfill()
            case .failure(let error):
                XCTAssertNil(error)
                exp.fulfill()
            }
        }
        waitForExpectations(timeout: 3.0)
        
    }
    
}
